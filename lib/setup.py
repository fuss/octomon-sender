#!/usr/bin/env python

from distutils.core import setup

setup(name='octomon',
      version='1.0',
      description='Octomon common library',
      author=['Enrico Zini','Christopher Gabriel'],
      author_email=['enrico@truelite.it', 'cgabriel@truelite.it'],
      url='https://devel.fuss.bz.it/wiki/OctoMon/',
      packages=['octomon'],
     )

import os
import os.path
from tempfile import mkstemp
from datetime import datetime

class Queue:
    def __init__(self, dir):
        if not os.path.isdir(dir):
            raise ValueError("%s is not a directory" % os.path.abspath(dir))
        self.dir = dir

    def empty(self):
        """
        Check if the queue is empty
        """
        for name in os.listdir(self.dir):
            if name.endswith(".msg"):
                return False
        return True

    def list(self):
        """
        Generate the names of the messages in the queue
        """
        for name in os.listdir(self.dir):
            if name.endswith(".msg"):
                yield name

    def add(self, buffer):
        """
        Adds a message to the queue.
        """
        now = datetime.now()
        (fd, name) = mkstemp(".msg", now.strftime("%Y%m%d-%H%M%S-"), self.dir)
        fd = os.fdopen(fd, "wb")
        fd.write(buffer)
        fd.close()
        return os.path.basename(name)

    def read(self, name):
        """
        Reads a message from the queue.
        """
        path = os.path.join(self.dir, name)
        try:
            fd = open(path, "rb")
        except IOError:
            return None
        return fd.read()

    def remove(self, name):
        """
        Removes a message from the queue.
        """
        os.unlink(os.path.join(self.dir, name))

    def validate(self, name):
        """
        Return true if name is a name that exists in the queue.
        """
        print(name)
        print(os.listdir(self.dir))
        return name in os.listdir(self.dir)

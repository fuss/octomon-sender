from .encoder import Encoder
from .decoder import Decoder
from .message import Message
from .queue import Queue
from .octomonmessage import OctomonMessage

def prettyFingerprint(fpr):
    res = []
    for i, c in enumerate(fpr):
        if i != 0:
            if i % 4 == 0:
                res.append(' ')
            if i % 20 == 0:
                res.append(' ')
        res.append(c)
    return ''.join(res) 

import unittest
from octomon import Queue, Encoder, Decoder, Message
from tempfile import mkdtemp
from shutil import rmtree
from io import StringIO
from gzip import GzipFile

class TestQueue(unittest.TestCase):
    def setUp(self):
        # Create the encoder and the decoder with the right keys
        self.encoder = Encoder('certs/server.pem', 'certs/client.pem', 'certs/client_key.pem')
        self.decoder = Decoder("certs/server.pem", "certs/server_key.pem")

        self.dirname = mkdtemp()
        self.queue = Queue(self.dirname)

    def tearDown(self):
        rmtree(self.dirname)

    def makeTestMessage(self):
        msg = Message()

        msg.setCertificate(open("certs/client.pem").read())

        buf = StringIO()
        print("School: test", file=buf)
        print("Version: test", file=buf)
        msg["metadata.txt"] = buf.getvalue()

        buf = StringIO()
        print("host1,name1,value1,123.456", file=buf)
        print("host2,name2,value2,234.567", file=buf)
        msg["data.csv"] = buf.getvalue()

        return msg
            
    def testEmpty(self):
        self.assertEqual(self.queue.empty(), True)

        names = [x for x in self.queue.list()]
        self.assertEqual(names, [])

        self.assertEqual(self.queue.read("missing"), None)

    def testOne(self):
        # Add an element and make sure that we can read it
        msg = self.makeTestMessage()
        self.queue.add(self.encoder.cleartext(msg))

        self.assertEqual(self.queue.empty(), False)

        names = [x for x in self.queue.list()]
        self.assertEqual(len(names), 1)

        msg1 = Message(self.queue.read(names[0]))
        self.assertEqual(msg1, msg)
        
        self.queue.remove(names[0])
        names = [x for x in self.queue.list()]
        self.assertEqual(names, [])

    def testValidate(self):
        name = self.queue.add("testtesttest")
        self.assertEqual(self.queue.validate(name), True)
        self.assertEqual(self.queue.validate("../"+self.dirname+"/"+name), False)
        self.assertEqual(self.queue.validate("./"+name), False)
        self.assertEqual(self.queue.validate("../../../../etc/passwd"), False)

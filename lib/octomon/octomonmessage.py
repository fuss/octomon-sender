from .message import Message
import M2Crypto as m2
import csv

def _x509NameToDict(name):
    res = dict()
    for k in name.nid:
        val = getattr(name, k, None)
        if val != None:
            res[k] = val
    return res

class OctomonMessage(Message):
    def certinfo(self):
        """
        Get a dictionary with information about the message
        """
        res = dict()
        c = m2.X509.load_cert_string(self.certificate())
        res["_version"] = c.get_version()
        res["_serial"] = c.get_serial_number()
        res["_notbefore"] = str(c.get_not_before())
        res["_notafter"] = str(c.get_not_after())
        res["_issuer"] = _x509NameToDict(c.get_issuer())
        #res["_issuer"] = [x.get_data().as_text() for x in c.get_issuer()]
        res["_subject"] = _x509NameToDict(c.get_subject())
        #res["_subject"] = [x.get_data().as_text() for x in c.get_subject()]
        for i in range(c.get_ext_count()):
            ext = c.get_ext_at(i)
            res[ext.get_name()] = ext.get_value()
        return res

    def setMetadata(self, val):
        """
        Set the metadata for this message
        """
        self["metadata.txt"] = val

    def setData(self, val):
        """
        Set the data for this message
        """
        self["data.csv"] = val

    def metadata(self):
        """
        Generate the metadata lines
        """
        return self.fdget("metadata.txt").__iter__()

    def tuples(self):
        """
        Generate the (host, key, value, timestamp) tuples of the message data
        """
        for host, key, val, ts in csv.reader(self.fdget("data.csv")):
            yield host, key, val, float(ts)

#!/usr/bin/env python

from distutils.core import setup

setup(name='OctomonSender',
      version='10.1',
      description='Octomon sender',
      author=['Enrico Zini','Christopher Gabriel','Marco Marinello'],
      author_email=['enrico@truelite.it', 'cgabriel@truelite.it', 'mmarinello@fuss.bz.it'],
      url='https://devel.fuss.bz.it/wiki/OctoMon/',
      install_requires = [
          "octomon",
      ],
      scripts=['octomon-sender'],
     )

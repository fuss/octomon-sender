import unittest
from sender import DB

class TestDB(unittest.TestCase):
    def setUp(self):
        """
        Create an empty in-memory database to use for tests
        """
        self.db = DB(":memory:")

        cu = self.db.db.cursor()
        cu.execute("create table client_activity (hostname text not null, timestamp float not null)");
        cu.execute("create table client_components (hostname text not null, component text not null, value text not null, timestamp float not null)")
        self.db.db.commit()

        self.sequence = 0
    
    def populate(self):
        """
        Populate the database with 5 client_activity and 5 client_components rows.

        The timestamps are always increasing, even across different populate() calls.
        """
        cu = self.db.db.cursor()

        for i in xrange(5):
            self.sequence += 1
            cu.execute("INSERT INTO client_activity VALUES (%s, %f)",
                "host%d" % i,
                self.sequence)
            cu.execute("INSERT INTO client_components VALUES (%s, %s, %s, %f)",
                "host%d" % i,
                "comp%d" % i,
                "val%d" % (self.sequence),
                self.sequence)

        self.db.db.commit()
            
    def testEvrange(self):
        "Test the boundary behaviour of DB.evrange"
        self.populate()

        # Check if the extremes are included
        timestamps = []
        for a, b, c, d in self.db.evrange(1, 3):
            timestamps.append(d)
        timestamps.sort()
        self.assertEquals(timestamps, [1.0, 1.0, 2.0, 2.0])

        # Check if all the rest of the data comes out
        timestamps = []
        for a, b, c, d in self.db.evrange(3, 10):
            timestamps.append(d)
        timestamps.sort()
        self.assertEquals(timestamps, [3.0, 3.0, 4.0, 4.0, 5.0, 5.0])

    def testSendLog(self):
        "Test the behaviour of the send log"
        self.populate()

        # At the beginning, the send log is empty
        lastsend = self.db.lastsend()
        self.assertEquals(lastsend, 0.0)

        # Perform an extraction
        count = 0
        for x in self.db.evrange(lastsend, 6):
            count += 1
        self.assertEquals(count, 10)

        # Log the successful extraction
        self.db.log_successful_send(lastsend, 6, 10)

        # Check what is logged
        log = [x for x in self.db.list_sends()]
        self.assertEquals(len(log), 1)
        self.assertEquals(log[0][0], lastsend)  # Start
        self.assertEquals(log[0][1], 6)         # End
        self.assertEquals(log[0][2], 10)        # Lines

        # Check that the next send will start from where we got
        self.assertEquals(self.db.lastsend(), 6)

        # See that we can parse references to past sends
        res = self.db.parse_resend("-1")
        self.assertEquals(res, (lastsend, 6))

        res = self.db.parse_resend("6")
        self.assertEquals(res, (lastsend, 6))
